Training the model
===

* **URL**

	https://president-api.eu-gb.mybluemix.net/train

* **Method:**
  
	  `POST`

* **Data Params**
 
 	**Header**
 
 	 * Authorization : <username>:<password>

     * Content-Type : application/json
 
 	**Body**

    	{
    	"training_data": [2d List of Integers]
    	"labels": [List of Strings]
   		}



* **Success Response:**

	**Code: ** 200 
	
 	**Content:** {"message": "training done" }
    	
 
* **Sample body:**
 
		{
		"training_data": [[1,0,1,4,3,1,1,1,0],[0,0,1,8,2,1,0,0,1],[1,1,1,3,0,0,1,1,1]]
		"labels": ["Yes","No", "Yes"]
		}

