Sign_in
===

* **URL**

	https://president-api.eu-gb.mybluemix.net/sign_in

* **Method:**
  
	  `POST`

* **Data Params**
 
 	**Header**
 
 	Content-Type : application/json
 
 	**Body**

		{
			"username": <username>,
			"password": <password>
		}
   	

* **Success Response:**

	**Code: ** 200 
	
 	**Content:** {
    				"password": username,
    				"username": password
				 }
    	

* **Sample body:**
 
		{
			"username": "John",
			"password": "Doe"
		}

