![text](https://i.gyazo.com/87f283c727a8d33f5d90683da63b4b48.png)
# Craftworkz: IoT & AI challenge
![text](https://d.ibtimes.co.uk/en/full/1576636/end-of-the-world-prediction.jpg)

We are 2030 and 766 million people are on the move. They have been surprised by how fast the world has been changing lately. Massive terrorist attacks, the rise of cryptocurrencies & political disorder divided the world into many groups. The rich are concentrated in metropoles protected by an army of robots. The middle class tries to continue their lives, neglecting the fact that they are about to lose it all. The growing number of poor ones are on the move in search for a better life. They all hope for a better future, they hope for a better world.

A selected group of people, including you, are locked in a bunker with the charge of bringing order back to world. Together, you have decided that there are 3 necessary elements to achieve your goal: experienced ministers, a good president and an effective defensive weapen.

![text](https://static1.squarespace.com/static/57f6c3bd8419c2af7bf55217/t/57fd557e1b631b13d85c5ab6/1476220287195/underground-bunker.jpg?format=1000w)
## Challenge 1: Choosing the ministers
![text](https://media-exp2.licdn.com/mpr/mpr/AAEAAQAAAAAAAAPXAAAAJDkzZjIxNWJhLTgzNzQtNGUwMC1iNWM0LTZmMDBmMTRkNWE2OA.jpg)

Let's start by choosing the right ministers. 30 years ago, there where 4 ministers who where loved by everyone and did fantastic work. Ofcourse we want them back on the job but there are a lot of people prentending to be them. Fortunately, we have old photo's of the 4 ministers and a present picture of all the persons saying to be them. You can now choose the correct person with the assistent of AI. 

### Cognitive services Microsoft Azure

To do face recognition and comparision you are going to use one of the cognitive services of Microsoft Azure. These services make different API's available to developers. With the use of these API's you can write powerfull AI applications with a little knowledge of artificial intelligence. You can discover all its capabilities here:

[Cognitive services](https://azure.microsoft.com/en-us/services/cognitive-services/)

### Face API

The API that is most interesting for this challenge is the Face API: 

[Face API](https://azure.microsoft.com/en-us/services/cognitive-services/face/)

It conists of serveral endpoints for which the documentation can be found here:

[Face API Reference](https://westus.dev.cognitive.microsoft.com/docs/services/563879b61984550e40cbbe8d/operations/563879b61984550f30395236)

If you rather use the python SDK instead of sending the requests yourself, check this out:

[Face API Python SDK](https://github.com/Microsoft/Cognitive-Face-Python)

Before you can use the face API, you need an access key. A free key can be obtained here:

[Face API Key](https://azure.microsoft.com/en-us/try/cognitive-services/?api=face-api)

With this free key you can only do 20 transactions per minute so pay attention to not exceed this limit.

### What to do for this challenge

With the info you found above, write some code for reading the old photo of one minister and all the pictures of its corresponding collection into the system. The system then has to find and display the person in the collection that has the biggest chance of being the old minister. TIP: You need to use 2 different endpoints of the face API.

The 4 old photo's can be found here:

[Old_ministers](https://photos.google.com/share/AF1QipOmp5klHz8gNcmf94A0nn02sFbW5cCSr4TFpmX9JQwLez6vHeHPdoy4_dVyYijVEA?key=cGQ1N2MybTlKYi1Qb1FEdGNZbFBZNmxRTWNiRGdB)

and the collections of the present persons here:

[People pretending to be minister 1](https://drive.google.com/drive/folders/1eb2mZOnnAu2zNCrxxnHrgVNT8cFQbMZM)

[People pretending to be minister 2](https://drive.google.com/drive/folders/1SU5uBn0rITf4xPSRvqbmj4ErhoPm7DHD)

[People pretending to be minister 3](https://drive.google.com/drive/folders/1gaArGEf0DtVtNOv8V8Fqaj8vHBrIy3CR)

[People pretending to be minister 4](https://drive.google.com/drive/folders/12AsD-TXe9G-736tWyOSYJ3FYrIUdrJVo)


Cool, we found our ministers. Let's move to challenge 2.

## Challenge 2: Choosing the president

![text](https://uploads.toptal.io/blog/image/443/toptal-blog-image-1407508081138.png)

Now it's time to decide who our next president is going to be. We can't risk to pick a bad one, so how can we be sure ? Let a computer decide it automatically !

### Training the model

First the computer program has to learn the characteristics of a good president based on some examples. This is called the training phase. In the training phase a machine learning algorithm takes a bunch of examples and calculates a model of it. The examples you can use are found in the [training_data.csv](https://bitbucket.org/iBizz/crw_htf_challenge/src/0cc4134d423980a5e5eebae948f5c01965128319/training_data.csv%20?at=master&fileviewer=file-view-default) file.

There are several machine learning algorithms to choose from. We implemented a REST API that uses support vector machines for this purpose. This is the documentation for the training endpoint of our API: [training_president_doc.md](training_president_doc.md)

First make an account for this API: [sign_in_doc.md](sign_in_doc.md). You only has to do this once. 

Before you can use this endpoint to train the model, you have to prepare the training data. As you can see in the documentation you need a 2 dimensional list/array with a vector representation of the characteristics and a list with the labels. 

So first write a program to parse the csv file into these 2 lists. The 2d list contains a list for each past president (each line in the csv file), this list is the vector representation of that president. The vector representation can only contain numerical data so you have to translate the catagorical  variables into integers. Do this like in the following example:

> Example: We represent a characteristic that can take 4 different values: A, B, C and D
> as a list with 3 elements. the 4 possibilities are then:

> A: [1,0,0]

> B: [0,1,0]

> C: [0,0,1]
 
> D: [0,0,0] 

A whole example of parsing the XML file:

> XML:
> 
> | name   | age | Hair color | Smart | Good president|
|--------|-----|------------|-------|-------|
| name 1 | 20  | Black      | yes   | yes |
| name 2 | 10  | Brown      | no    |yes |
| name 3 | 30  | Blue       | yes   |no |
>
> Training_data: [[20,1,0,1],[10,0,1,0],[30,0,0,1]]
> 
> labels: ["yes", "yes", "no"]

A list of all categorical variables with there possible values can be found in [description file](description_file.txt)

Now you can make a request in your code to the train endpoint of the API with the parsed data in the body.

### Predicting 
Congratulations, you trained a model that can predict if a person is going to be a good president or not. Expand you code by doing a request to the [predict](predict_president_doc.md) endpoint of our API for each of the persons listed in [File of possible presidents](https://bitbucket.org/iBizz/crw_htf_challenge/src/f2e35c1b606c47663f41b25a3bde8845c7793d80/potentialPresidents.csv?at=master&fileviewer=file-view-default) and returning the name of the person most likely to be a good president (The person, who is predicted to be a good president, with the highest distance) according to your model.

## Challenge 3: Controlling a defensive weapon

![text](https://financialtribune.com/sites/default/files/field/image/17january/11_mr_iot_500-ed.jpg)

We figuered our gouverment out, now we need to defense ourselves against the robot army of the rich. Since we don't want to lose more people, we designed a canon that be controlled remotely with IoT. 
If we manage to make a working canon, we could finally fight back against the robot army.

So what do we need? We need a good controlpanel where we can remotely aim and shoot the canon. 

### Particle 
Particle includes everything you need to deploy an IoT product: a device cloud platform and connectivity hardware. Those two are already available. Still we need to call the functions so it could be activated from the panel.  
If you are interested about [how Particle works](https://docs.particle.io/guide/getting-started/intro/photon/)

We have a photon that it connected to the internet. To send calls to the Photon, you'll have to use the Particle SDK (if you use javascript) or the Cloud Api (if you use Python). 
[Here is the documentation](https://docs.particle.io/reference/api/)

To connect with the right photon, you'll need some credentials to login with the SDK or API and so you can get the deviceId.
The credentials are: 

* aagje.reynders@craftworkz.be
* htf123

When you have your deviceId, you can now control the canon. If you are ready to start testing the canon, please give a sign, so we have a overview of who is testing the canon. 

There are two functions you can call. With name "shoot" and "setPosition". Shoot doesn't need a parameter, but setPosition does. It needs to be an int from 0-180. It's for aiming the device from a certain angle. If you want to know what the current position is you can get with the variable "pos". 
Try to write a function so the canon slowly rises/set.  
If you shoot and the green led turns on, you know the canon works. 

## Challenge 4: Putting it all together 

The other people in the bunker want your program a bit more user friendly. They don't want to use the terminal or an IDE to run the programs.

### API 

Create an API (or multiple) on top of all the code you just wrote. You can choose every approach you want.

### Web interface

Make a web interface in any framework you want to combine the elements on one page. You have to be able to 

* upload the images on the page and show the choosen ministers on the page
* upload the training data and the possible president and show the name of the best president on the screen
* press on buttons to control the canon


## Challenge 5: Controlling with voice

### Natural language classifier 

What we want for this challenge is a natural language classifier. Such a classifier labels sentences with a specific class. Let's say the two possible classes are 'rise' and 'fire'. The sentence "Hi there! Could you maybe rise a little bit ?" will classify to 'rise'. There are two ways to build this. You can choose either one, but bonus points if you use the Watson approach ;-)
#### A simple natural language classifier
You can build a very simple classifier. Such a classifier looks at the sentences, looks at the different words in the sentence and if the word 'rise' appears in the sentence, it classifies the sentence as 'rise'. If the word 'fire' appears in the sentence, the classifier classifies the sentence as 'fire'.

#### Watson Natural Language Classifier
IBM Watson has a Natural Language Classifier service. Train this classifier with a few sentences that label to 'rise' and sentences that label to 'fire'. The documentation for the Watson Natural Language Classifier can be found here: [IBM watson NLP](https://www.ibm.com/watson/developercloud/nl-classifier.html)

#### Create an IBM Bluemix account
Go to http://console.eu-gb.bluemix.net to create a new Bluemix account. The first 30 days you try out Bluemix are completely free, so you can create and use the services you need.

### Frontend
Provide a little text field in the frontend where you can input your sentence, this will get classified by the classifier and the right command will be sent to particle.


### voice recognition
A natural language classifier did seem a little bit useless perhaps. Clicking on a button is faster and easier than entering a sentence. But what if you could speak to the system with your voice? For this challenge we will use [Watson Speech-to-Text](https://www.ibm.com/watson/services/speech-to-text/). Provide a button in the frontend to record a message, send it to Watson, get the text from the speech and connect this text to your Natural language classifier.

Wow, that's it! You did a great job! Thank you for building a new world.


