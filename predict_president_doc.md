Predicting
===

* **URL**

	https://president-api.eu-gb.mybluemix.net/predict

* **Method:**
  
	  `POST`

* **Data Params**
 
 	**Header**
    
	 * Authorization : <username>:<password>
     
     * Content-Type : application/json
 
 	**Body**

    	{
	     "person": [List]
   		}

* **Success Response:**

	**Code: ** 200 
	
 	**Content:** {
                  "distance": distance,
                  "prediction": label
                 }
    	
 
* **Sample body:**
 
		{
		"person": [0,0,1,8,2,1,0,0,1]
		}

